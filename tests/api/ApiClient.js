const request = require('request-promise');

class ApiClient {
    constructor({ baseUrl }) {
        this.baseUrl = baseUrl;
    }

    post(path, data) {
        return request({
            method : 'POST',
            body   : data,
            uri    : this.baseUrl + path,
            json   : true
        });
    }

    async get(path) {
        const res = await request({
            method : 'GET',
            uri    : this.baseUrl + path
        });

        return JSON.parse(res);
    }

    async delete(path) {
        const res = await request({
            method : 'DELETE',
            uri    : this.baseUrl + path
        });

        return JSON.parse(res);
    }
}

module.exports = ApiClient;
