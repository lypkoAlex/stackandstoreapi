/* eslint-disable */
const assert = require('assert');
const ApiClient   = require('./api/ApiClient');

const app          = require('../server.js');
const { appPort, apiPrefix }  = require('../etc/config.json')[process.env.ENV];

const api = new ApiClient({ baseUrl : `http://localhost:${appPort}${apiPrefix}/`});
const first = {
    item : 'first'
};
const second = {
    item : 'second'
}
let server;
describe('Stack', () => {
    before(() => {
        server = app.listen(appPort);
    });

    describe('testing LIFO structure', () => {
        it('push items', async () => {
            const resFirst = await api.post('stack', first);
            const resSecond = await api.post('stack', second);
            assert.equal(resFirst.status, 1);
            assert.equal(resSecond.status, 1);
        });

        it('pop items', async () => {
            const resSecond = await api.get('stack');
            assert.equal(resSecond.status, 1);
            assert.equal(resSecond.data.item, 'second');

            const resFirst = await api.get('stack');
            assert.equal(resFirst.status, 1);
            assert.equal(resFirst.data.item, 'first');
        });

        it('pop from empty stack', async () => {
            const res = await api.get('stack');
            assert.equal(res.status, 1);
            assert.equal(res.data.item, undefined);
        });
    });

    after(() => {
        server.close();
    })
});
