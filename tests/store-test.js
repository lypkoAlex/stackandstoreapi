/* eslint-disable */
const assert = require('assert');
const ApiClient   = require('./api/ApiClient');

const app          = require('../server.js');
const { appPort, apiPrefix }  = require('../etc/config.json')[process.env.ENV];

const api = new ApiClient({ baseUrl : `http://localhost:${appPort}${apiPrefix}/`});
const first = {
    key  : 'first',
    value : 'first'
};
const second = {
    key  : 'second',
    value : 'second'
}

describe('Store', () => {
    before(() => {
        app.listen(appPort);
    });

    describe('testing memory-store structure', () => {
        it('set items', async () => {
            const resFirst = await api.post('store/items', first);
            const resSecond = await api.post('store/items', second);
            assert.equal(resFirst.status, 1);
            assert.equal(resSecond.status, 1);
        });
        
        it('get item', async () => {
            const resFirst = await api.get(`store/items/${first.key}`);

            assert.equal(resFirst.status, 1);
            assert.equal(Object.keys(resFirst.data)[0], 'first');
            assert.equal(resFirst.data.first, 'first');
        });

        it('delete item', async () => {
            const res = await api.delete(`store/items/${first.key}`);
            const resFirst = await api.get(`store/items/${first.key}`);
            assert.equal(resFirst.status, 1);
            assert.equal(resFirst.data.item, undefined);
            assert.equal(res.status, 1);
        });
    });
});
