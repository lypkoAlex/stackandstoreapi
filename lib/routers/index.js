const Stack = require('./Stack.js');
const Store = require('./Store.js');

module.exports = function init() {
    return {
        stack : new Stack(),
        store : new Store()
    };
};
