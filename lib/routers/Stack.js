const Base = require('./Base');

module.exports = class Fules extends Base {
    async pop() {
        return this.run('stack/Pop', {
            params : {}
        });
    }

    async push({ body}) {
        return this.run('stack/Push', {
            params : {
                ...body
            }
        });
    }
};
