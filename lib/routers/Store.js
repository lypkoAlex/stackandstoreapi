const Base = require('./Base');

module.exports = class Fules extends Base {
    async delete({ params }) {
        return this.run('store/Delete', {
            params
        });
    }

    async set({ body }) {
        return this.run('store/Set', {
            params : {
                ...body
            }
        });
    }

    async get({ params}) {
        return this.run('store/Get', {
            params : {
                ...params
            }
        });
    }
};
