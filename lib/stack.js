class Stack {
    constructor() {
        this.state = [];
    }
    push(element) {
        this.state.push(element);
    }

    pop() {
        return this.state.pop();
    }
}

module.exports = new Stack();
