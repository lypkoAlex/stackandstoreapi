class MemoryStore {
    constructor() {
        this.state = new Map();
        this.deletionTimers = new Map();
    }

    set(key, value, expiration) {
        const existedTimerId = this.deletionTimers.get(key);

        if (existedTimerId) {
            clearTimeout(existedTimerId);
            this.deletionTimers.delete(key);
        }

        if (expiration !== undefined) {
            const timoutId = setTimeout(() => {
                this.state.delete(key);
                this.deletionTimers.delete(key);
            }, expiration * 1000);

            this.deletionTimers.set(key, timoutId);
        }

        return this.state.set(key, value);
    }

    get(key) {
        return this.state.get(key);
    }

    delete(key) {
        this.deletionTimers.delete(key);

        return this.state.delete(key);
    }
}

module.exports = new MemoryStore();
