const stack = require('./stack');
const store = require('./store');

module.exports = {
    stack,
    store
};
