const Get    = require('./Get');
const Set    = require('./Set');
const Delete = require('./Delete');

module.exports = {
    Set,
    Get,
    Delete
};
