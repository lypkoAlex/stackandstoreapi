const Base          = require('../Base');
const memoryStorage = require('../../memoryStore');

module.exports =  class Set extends Base {
    validate(params) {
        const rules = {
            key : ['required']
        };

        return this.validator.validate(params, rules);
    }

    execute({ key }) {
        return { data :  { [key] : memoryStorage.get(key) } };
    }
};
