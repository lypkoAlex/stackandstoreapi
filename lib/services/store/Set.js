const Base          = require('../Base');
const memoryStorage = require('../../memoryStore');

module.exports =  class Set extends Base {
    validate(params) {
        const rules = {
            key        : ['required'],
            value      : ['required'],
            expiration : ['positive_integer']
        };

        return this.validator.validate(params, rules);
    }

    execute({ key, value, expiration }) {
        memoryStorage.set(key, value, expiration);

        return {};
    }
};
