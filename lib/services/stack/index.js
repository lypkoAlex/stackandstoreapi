const Pop  = require('./Pop');
const Push = require('./Push');

module.exports = {
    Push,
    Pop
};
