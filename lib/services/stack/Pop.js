const Base = require('../Base');
const stack = require('../../stack.js');

module.exports =  class Pop extends Base {
    validate(params) {
        const rules = {};

        return this.validator.validate(params, rules);
    }

    async execute() {
        const result = {
            data : {
                item : stack.pop()
            }
        };

        return result;
    }
};
