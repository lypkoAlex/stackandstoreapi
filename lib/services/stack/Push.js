const Base = require('../Base');
const stack = require('../../stack.js');


module.exports =  class Push extends Base {
    validate(params) {
        const rules = {
            item : ['required', 'string']
        };

        return this.validator.validate(params, rules);
    }

    async execute(data) {
        stack.push(data.item);
        return {};
    }
};
