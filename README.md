STACK AND STORE API
------------------------------------------------------

### START
clone this repo
### Installation and run
```sh
$ npm install
$ npm start
```
### Run test
```sh
$ npm run test
```

### Application structure
```
server.js
 /etc
 /tests
 /lib
    /routes
    /services
    stack.js
    memoryStore.js
```

**/etc** - config files.

**/server.js** - main file with http server.

**/lib/tests** - api tests for aplication.

**/lib** - contains application source code.

**/lib/routes** - routes.

**/lib/services** - business logic of application.

**/lib/memoryStore.js** - implementation for memory store with expiration time.

**/lib/stack.js** - implementation for stack.


