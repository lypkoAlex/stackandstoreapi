const express     = require('express');
const bodyParser  = require('body-parser');
const cors        = require('cors');
const { appPort, apiPrefix }   = require('./etc/config.json')[process.env.ENV];
const promiseRouter = require('./lib/PromiseRouter.js');
const getRoutes     = require('./lib/routers');
const routes        = getRoutes();
const router        = promiseRouter(express.Router);

const app = express();

app.use(bodyParser.urlencoded());

app.use(cors({ origin: '*' }));

app.use(bodyParser.json({limit: 1024 * 1024, verify: (req, res, buf) => {
    try {
        JSON.parse(buf);
    } catch (e) {
        res.send({
            status: 0,
            error: {
                code:    'BROKEN_JSON',
                message: 'Please, verify your json'
            }
        });
    }
}}));

app.use(apiPrefix, router);

router.postAsync('/stack', routes.stack.push.bind(routes.stack));
router.getAsync('/stack', routes.stack.pop.bind(routes.stack));

router.postAsync('/store/items', routes.store.set.bind(routes.store));
router.getAsync('/store/items/:key', routes.store.get.bind(routes.store));
router.deleteAsync('/store/items/:key', routes.store.delete.bind(routes.store));

if (process.env.ENV === 'live') {
    app.listen(appPort, function () {
        console.log(`listening on port ${appPort}`);
    });
}

module.exports = app;
